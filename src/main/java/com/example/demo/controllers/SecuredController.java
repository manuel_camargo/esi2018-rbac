package com.example.demo.controllers;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/services")
@CrossOrigin
public class SecuredController {

    @GetMapping("/resource1")
    @Secured({"ROLE_ADMIN", "ROLE_USER1"})
    public String getResource1() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        System.out.println(username);
        return "{\"msg\": \"Hey, welcome to resource1\"}";
    }

    @GetMapping("/resource2")
    @Secured({"ROLE_USER2"})
    public String getResource2() {
        return "{\"msg\": \"Hey, welcome to resource2\"}";
    }

}
